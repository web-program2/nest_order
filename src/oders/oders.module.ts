import { Module } from '@nestjs/common';
import { OdersService } from './oders.service';
import { OdersController } from './oders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/oder.entity';
import { OrderItem } from './entities/oder-item.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderItem, Customer, Product])],
  controllers: [OdersController],
  providers: [OdersService],
})
export class OdersModule {}
